import tarfile
import zipfile

def extract(path, file_name):
    """
    extract a compressed file to the given path.
    """
    if (file_name.endswith("tar.gz")):
        tar = tarfile.open(file_name, "r:gz")
        tar.extractall(path)
        tar.close()

    elif file_name.endswith("zip"):
        zip_ref = zipfile.ZipFile(path_to_zip_file, 'r')
        zip_ref.extractall(directory_to_extract_to)
        zip_ref.close()
    